# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cargo

DESCRIPTION=""
HOMEPAGE="https://github.com/kamiyaa/joshuto"
SRC_URI="https://github.com/kamiyaa/joshuto/archive/${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="
	sys-libs/ncurses
	dev-lang/rust[cargo]"
RDEPEND="${DEPEND}"
BDEPEND=""
