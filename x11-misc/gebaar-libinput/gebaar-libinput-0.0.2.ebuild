# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake-utils

DESCRIPTION="A Super Simple WM Independent Touchpad Gesture Daemon for libinput"
HOMEPAGE="https://github.com/Coffee2CodeNL/gebaar-libinput"
SRC_URI="https://github.com/Coffee2CodeNL/gebaar-libinput/archive/v${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="x11-drivers/xf86-input-libinput"
RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
	default
}

src_install() {
	default
}
