# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit meson

DESCRIPTION="A compositor for X11"
HOMEPAGE="https://github.com/yshui/compton"
SRC_URI="https://github.com/yshui/compton/archive/v${PV}.tar.gz"

LICENSE="MPL-2.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE="dbus +drm opengl +pcre xinerama"

DEPEND="
	dev-libs/libconfig
	x11-libs/libX11
	x11-libs/libxcb
	x11-libs/libXext
	x11-base/xcb-proto
	x11-base/xorg-proto
	x11-libs/xcb-util-renderutil
	x11-libs/xcb-util-image
	x11-libs/pixman
	dev-libs/libev
	dev-libs/libxdg-basedir
	x11-apps/xprop
	x11-apps/xwininfo
	app-text/asciidoc
	virtual/pkgconfig
	dbus? ( sys-apps/dbus )
	drm? ( x11-libs/libdrm )
	opengl? ( virtual/opengl )
	pcre? ( dev-libs/libpcre:3 )
	xinerama? ( x11-libs/libXinerama )"
RDEPEND="${DEPEND}"
BDEPEND=""
