# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="RetroArch is a frontend for emulators, game engines and media players."
HOMEPAGE="https://www.retroarch.com"
SRC_URI="https://github.com/libretro/RetroArch/releases/download/v${PV}/RetroArch-${PV}.tar.xz"
S="${WORKDIR}/RetroArch-${PV}"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
	./configure --prefix=/usr --docdir=/usr/share/doc
}
