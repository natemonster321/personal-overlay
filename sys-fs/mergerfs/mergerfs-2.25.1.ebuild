# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Another (FUSE based) union filesystem"
HOMEPAGE="https://github.com/trapexit/mergerfs"
SRC_URI="https://github.com/trapexit/mergerfs/releases/download/2.25.1/mergerfs-${PV}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="
	sys-apps/attr:=
	>=sys-apps/util-linux-2.18
	sys-devel/gettext:="
RDEPEND="${DEPEND}"
BDEPEND=""

#TODO: add use-flag for pandoc for manpages

src_install() {
	emake DESTDIR="${D}" PREFIX="/usr" install
	einstalldocs
}
